 An event planner needs to store all the events for one year from his calendar, 
 in order to keep track of the upcoming events, sorted by their date. 
 The application will allow to add an event, remove an event, display all the events in the calendar and display all the events from a given date. 
 It will use a sorted multimap implemented on a hash table, where each key represents the date of a future event.