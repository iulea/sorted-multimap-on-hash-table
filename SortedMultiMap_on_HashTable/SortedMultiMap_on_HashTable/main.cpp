#include "SortedMultiMap.h"
#include "UI.h"
#include "Teste.h"
#include <Windows.h>

int main()
{
	system("color f4");
	Teste tests{};
	SMM smm{};
	smm.init();
	tests.testSMM(smm);
	struct data k;
	k.zi = 9; k.luna = 7;
	smm.add(k, "retake exam - data structures and algorithms");
	k.zi = 19; k.luna = 6;
	smm.add(k, "exam - object oriented programming");
	smm.add(k, "practical exam - object oriented programming");
	k.zi = 25;
	smm.add(k, "exam - operating systems");
	k.zi = 30;
	smm.add(k, "exam - dynamic systems");
	k.zi = 13;
	smm.add(k, "exam - data structures and algorithms");
	UI ui{ smm };
	ui.runApplication();
}