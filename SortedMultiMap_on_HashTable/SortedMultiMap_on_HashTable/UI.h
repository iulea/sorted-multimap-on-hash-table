#pragma once
#include "SortedMultiMap.h"

class UI
{
private:
	SMM smm;;
public:
	//Constructor
	UI();

	//Copy constructor
	UI(const SMM& smm);

	//Destructor
	~UI();

	//Aplication control function
	void runApplication();

private:
	static void printProblemStatement();
	static void printMenuApplication();
};

