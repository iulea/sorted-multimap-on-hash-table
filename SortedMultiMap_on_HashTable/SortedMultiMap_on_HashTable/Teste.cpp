#include "Teste.h"


void Teste::testSMM(SMM smm)
{
	struct data k;
	k.zi = 1;
	k.luna = 2;
	std::string e = "eveniment";
	smm.add(k, e);
	e = "alt eveniment";
	struct data k2;
	k2.zi = 2;
	k2.luna = 1;
	smm.add(k2, e);
	Iterator it{ smm };
	Telem<struct data, std::string> elem = it.getCurrent();
	assert(elem.k.zi == 2);
	assert(elem.k.luna == 1);
	assert(elem.val == "alt eveniment");
	it.next();
	elem = it.getCurrent();
	assert(elem.k.zi == 1);
	assert(elem.k.luna == 2);
	assert(elem.val == "eveniment");
	assert(smm.getRelation().getRelation(k2, k) == -1);
	Tlist lista = smm.search(k2);
	Tnode* h = lista.head;
	assert(h->info.k.luna == 1 && h->info.k.zi == 2);
	assert(h->info.val == "alt eveniment");
	smm.remove(k2, e);
	Iterator it2{ smm };
	Telem<struct data, std::string> elem2 = it.getCurrent();
	assert(elem2.k.zi == 1);
	assert(elem2.k.luna == 2);
	assert(elem2.val == "eveniment");
}
