#pragma once
#include <iostream>

template <typename Tkey, typename Tvalue>
struct Telem {
	Tkey k;
	Tvalue val;
};

struct data {
	int zi;
	int luna;
};

struct Tnode {

	Telem <struct data, std::string> info;
	struct Tnode *next;
	struct Tnode *prev;

};

/*
function that compares 2 keys
input: k1, k2 - data
output: -1 if k1 is before k2, 1 if k1 is after k2, and 0 if the dates are the same day
*/
struct Relation {
	struct data k1, k2;
	Relation()
	{
		k1.zi = 0;
		k1.luna = 0;
		k2.zi = 0;
		k2.luna = 0;
	}
	int getRelation(struct data K1, struct data K2)
	{
		k1.zi = K1.zi;
		k2.zi = K2.zi;
		k1.luna = K1.luna;
		k2.luna = K2.luna;
		if (k1.luna < k2.luna)
			return -1;
		if (k1.luna > k2.luna)
			return 1;
		if (k1.luna == k2.luna)
		{
			if (k1.zi < k2.zi)
				return -1;
			if (k1.zi > k2.zi)
				return 1;
			if (k1.zi == k2.zi)
				return 1;
		}
	}
};

/*
hash function
input: k - data
output: h(k) = k.zi + k.luna
*/
struct Tfunction {
	struct data k;
	Tfunction()
	{
		k.zi = 0;
		k.luna = 0;

	}
	int getFunction(struct data k)
	{
		return k.luna + k.zi;
	}
};

struct Tlist {
	Tnode* head;
};

//-----------------------------------------------------------

class SMM
{
private:
	int m;
	Relation R;
	Tfunction h;
	Tnode * T[50];

	/*
	escription: searches for a key at the right position in the smm
	pre: k - data, i - int
	post: returns a list with all the elements that have k as a key
	*/
	Tlist searchNode(struct data k, int i);

	/*
	description: adds a new node to the smm, if the if there already are elemnts at the h(k) position
	pre: smm is a SMM, k is a TKey, v is an integer
	post: a new node with key k and value v is added to the smm. The order of the keys will respect the relation.
	*/
	void addANewNode(struct data k, std::string v);

public:
	friend class Iterator;


	//Constructor
	SMM() {};

	Relation getRelation() { return R; };

	void init();
	/*
	description: adds a pair <key, value> to the smm
	pre: k - data, v - string
	post: the pair was added into smm.
	*/
	void add(struct data k, std::string v);



	/*
	description: deletes a pair from the smm.
	pre: k -data, v - string
	post:the pair was deleted from smm (if it was in smm).
	*/
	void remove(struct data k, std::string v);

	/*
	description: searches for a key in the smm
	pre: k - data
	post: returns a list with all the elements that have k as a key, or an empty list otherwise
	*/
	Tlist search(struct data k);



	//Destructor
	~SMM() {};
};

//--------------------------------------------------------------

class Iterator
{
private:
	SMM smm;
	Tnode* current;
	Tlist ITL;
public:

	//Constructor
	Iterator(const SMM& smm) : smm{ smm } { this->smm = smm; this->current = new(Tnode); mergeLists(); this->current = ITL.head; };

	//Destructor
	~Iterator(){}
	
	Telem<struct data, std::string> getCurrent();
	void next();
	void prev();
	bool valid();
	Tnode* getTail();
	Tnode* getHead();

	/*
	merges the lists from each node in the vector from the smm, creating a list where the keys are sorted
	input: none
	output: ITL is a list which contains all the elements from the smm, sorted by their keys
	*/
	void mergeLists();
};