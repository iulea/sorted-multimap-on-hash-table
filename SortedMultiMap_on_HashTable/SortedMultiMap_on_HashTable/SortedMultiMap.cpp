#include "SortedMultiMap.h"

void SMM::init()
{
	this->m = 50;
	for (int i = 0; i < this->m; i++)
	{
		this->T[i] = NULL;
	}
}



void SMM::add(data k, std::string v)
{
	int i = this->h.getFunction(k);
	if (this->T[i] == NULL)
	{
		Tnode* newNode = new(Tnode);
		newNode->next = NULL;
		newNode->info.k.luna = k.luna;
		newNode->info.k.zi = k.zi;
		newNode->info.val = v;
		this->T[i] = newNode;
	}
	else
		//if T[i] already has elements
		addANewNode(k, v);
}

void SMM::addANewNode(data k, std::string v)
{
	Tnode* newNode = new(Tnode);
	newNode->info.k.luna = k.luna;
	newNode->info.k.zi = k.zi;
	newNode->info.val = v;
	int i = this->h.getFunction(k);
	Tnode* current = this->T[i];
	Tnode* prev = new(Tnode);
	while (current != NULL && this->R.getRelation(current->info.k, k) == -1)
	{
		prev = current;
		current = current->next;
	}
	//after the while, the new node has to be added between current and prev, in order to resprect the relation

	if (prev->next != current)
	{
		T[i] = newNode;
	}
	prev->next = newNode;
	newNode->next = current;

}

void SMM::remove(data k, std::string v)
{
	int i = this->h.getFunction(k);
	this->T[i]->prev = NULL;
	Tnode* current = this->T[i];
	Tnode* prev = new(Tnode);
	if (current->info.k.luna == k.luna && current->info.k.zi == k.zi && current->info.val == v)
		this->T[i] = this->T[i]->next;
	else
	{
		while (current != NULL)
		{
			if (current->info.k.luna == k.luna && current->info.k.zi == k.zi && current->info.val == v)
			{
				prev->next = current->next;
				if (current->next != NULL)
					current->next->prev = prev;

				free(current);
				break;
			}
			prev = current;
			current = current->next;
		}
	}
}

Tlist SMM::search(data k)
{
	Tlist lista;
	int i = this->h.getFunction(k);
	if (this->T[i] == NULL)
		lista.head = NULL;  // return an empty list 
	else
		lista = searchNode(k, i);
	return lista;

}

Tlist SMM::searchNode(data k, int i)
{
	Tnode* current = this->T[i];
	Tlist lista;
	lista.head = NULL;
	while (current != NULL)
	{
		if (current->info.k.zi == k.zi && current->info.k.luna == k.luna)
		{
			if (lista.head == NULL)
				lista.head = current;
			else
			{
				Tnode* urgh = lista.head;
				while (urgh->next != NULL)
					urgh = urgh->next;
				urgh->next = current;
				urgh->next->next = NULL;
			}
		}
		current = current->next;
	}
	return lista;
}

//---------------------------------------------------------------------

Telem<data, std::string> Iterator::getCurrent()
{
	return this->current->info;
}

void Iterator::next()
{
	if (valid())
		this->current = this->current->next;
}

void Iterator::prev()
{
	Tnode* prev = this->ITL.head;
	if (prev != NULL)
	{
		while (prev->next != NULL)
			prev = prev->next;
		prev->next = NULL;
	}
}


bool Iterator::valid()
{
	return this->current != NULL;
}

Tnode * Iterator::getTail()
{
	Tnode* t = this->ITL.head;
	while (t->next != NULL)
		t = t->next;
	return t;
}

Tnode * Iterator::getHead()
{
	return this->ITL.head;
}

void Iterator::mergeLists()
{
	Tnode* T[50];
	for (int i = 0; i < 50; i++)
	{
		T[i] = this->smm.T[i];
	}

	while (true)
	{
		Tnode* min = new(Tnode);
		min->info.k.luna = 13;
		min->info.k.zi = 32;
		min->next = NULL;
		int indice;
		for (int i = 0; i < 50; i++)
		{
			if(T[i] != NULL)
				if (this->smm.getRelation().getRelation(min->info.k, T[i]->info.k) == 1)
				{
					min->info = T[i]->info;
					min->next = T[i]->next;
					min->prev = T[i]->prev;
					indice = i;
				}
		}
		if (min->info.k.luna == 13)
			break;
		if (ITL.head == NULL)
		{
			ITL.head = min;
			T[indice] = T[indice]->next;
			ITL.head->next = NULL;
		}
		else
		{
			Tnode* current = this->ITL.head;
			while (current->next != NULL)
				current = current->next;
			T[indice] = T[indice]->next;
			current->next = min;
			current->next->next = NULL;
		}
	}
}
