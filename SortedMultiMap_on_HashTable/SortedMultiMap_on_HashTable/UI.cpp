#include "UI.h"
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <ctime>

using namespace std;

UI::UI()
{
}


UI::UI(const SMM& smm)
{
	this->smm = smm;
}


UI::~UI()
{
}

void UI::printMenuApplication()
{
	cout << "> Select a command: \n";
	cout << "     1. Insert an event. \n";
	cout << "     2. Remove an event. \n";
	cout << "     3. View all the events from a date. \n";
	cout << "     4. View all the events. \n";
}


void UI::runApplication()
{
	while (true)
	{
		printMenuApplication();
		int command = -1;
		int zi, luna;
		string eveniment;
		int val = 0;

		while (command == -1)
		{
			cout << "> ";
			cin >> command;
			if (command < 0 || command > 4) {
				cout << "> Unknown command '" << command << "'\n";
				command = -1;
			}
		}
		if (command == 1 || command == 2)
		{

			bool ok = true;

			cout << "> Event ( day - month - description): "; cin >> zi >> luna;
			std::getline(std::cin, eveniment);

			if (zi < 1 || zi > 31 || luna < 1 || luna > 12) {
				// Invalid input
				cout << "> Invalid date '" << zi << " " << luna << "'\n";
				ok = false; // Invalid user input.
				break;
			}
			if (!ok) {
				continue;
			}
		}
		if (command == 3)
		{

			bool ok = true;

			cout << "> Date ( day - month): "; cin >> zi >> luna;

			if (zi < 1 || zi > 31 || luna < 1 || luna > 12) {
				// Invalid input
				cout << "> Invalid date '" << zi << " " << luna << "'\n";
				ok = false; // Invalid user input.
				break;
			}
			if (!ok) {
				continue;
			}
		}
		if (command == 1) 
		{
			// Insert a value
			struct data k;
			k.zi = zi;
			k.luna = luna;
			smm.add(k, eveniment);

			cout << "> Event successfully added.\n";

		}
		else if (command == 3) 
		{
			//search for events form a given date
			struct data k;
			k.zi = zi;
			k.luna = luna;
			Tlist lista = smm.search(k);
			Tnode* e = lista.head;
			while (e != NULL)
			{
				cout << "		" << e->info.k.zi << " ";
				switch (e->info.k.luna) {
				case 1:
					cout << " January";
					break;
				case 2:
					cout << " February";
					break;
				case 3:
					cout << " March";
					break;
				case 4:
					cout << " April";
					break;
				case 5:
					cout << " May";
					break;
				case 6:
					cout << " June";
					break;
				case 7:
					cout << " July";
					break;
				case 8:
					cout << " August";
					break;
				case 9:
					cout << " September";
					break;
				case 10:
					cout << " October";
					break;
				case 11:
					cout << " November";
					break;
				case 12:
					cout << " December";
					break;
				}
				cout << " | " << e->info.val << endl;
				e = e->next;
			}
		}
		else if (command == 2)
		{
			// Remove a value
			struct data k;
			k.zi = zi;
			k.luna = luna;
			smm.remove(k, eveniment);

			cout << "> Event successfully removed.\n";

		}
		else if (command == 4)
		{
			// View events
				Iterator it{ smm };
				while (it.valid())
				{

					Telem<struct data, std::string> e = it.getCurrent();
					cout << "		" << e.k.zi << " ";
					switch (e.k.luna) {
					case 1:
						cout << " January";
						break;
					case 2:
						cout << " February";
						break;
					case 3:
						cout << " March";
						break;
					case 4:
						cout << " April";
						break;
					case 5:
						cout << " May";
						break;
					case 6:
						cout << " June";
						break;
					case 7:
						cout << " July";
						break;
					case 8:
						cout << " August";
						break;
					case 9:
						cout << " September";
						break;
					case 10:
						cout << " October";
						break;
					case 11:
						cout << " November";
						break;
					case 12:
						cout << " December";
						break;
					}
					cout << " | " << e.val << endl;
					it.next();
				}
			
		}

	}
}
